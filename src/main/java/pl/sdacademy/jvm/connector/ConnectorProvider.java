package pl.sdacademy.jvm.connector;

import pl.sdacademy.jvm.CustomClassLoader;
import pl.sdacademy.jvm.Launcher;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConnectorProvider {
    public static BitcoinMarketConnector getBitcoinMarketConnector() {
        BitcoinMarketConnector result = null;
        Properties properties = new Properties();
        CustomClassLoader classLoader = new CustomClassLoader(ConnectorProvider.class.getClassLoader());
        InputStream inputStream = Launcher.class.getResourceAsStream("/some.properties");

        try {
            properties.load(inputStream);

            String className = properties.getProperty("connector");
            Class clazz = classLoader.loadClass(className);
            /*
            Ćwiczenie nr 2: Dodaj w some.properties zmienną "env"
             jeżeli wartość tej zmiennej będzie inna, niż "prod", niech annotacja nie będzie sprawdzana
             */

            String env = properties.getProperty("env");

            if (!env.equals("prod")) {
                boolean testOnly = clazz.isAnnotationPresent(DoNotUseOnProd.class);

                if (testOnly) {
                    throw new RuntimeException("Attempt to use test class!");
                }
            }

            result = (BitcoinMarketConnector) clazz.newInstance();
        } catch (IOException | IllegalAccessException | InstantiationException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return result;
    }
}
