package pl.sdacademy.jvm.connector;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import pl.sdacademy.jvm.connector.dto.BitmaszynaTickerReply;

import java.math.BigInteger;

public class BitMaszynaConnector implements BitcoinMarketConnector {
    @Override
    public BigInteger getBitcoinPrice() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<BitmaszynaTickerReply> responseEntity = restTemplate.getForEntity("https://bitmaszyna.pl/api/BTCPLN/ticker.json", BitmaszynaTickerReply.class);
        return BigInteger.valueOf((long) (responseEntity.getBody().getAverage()*100));
    }
}
