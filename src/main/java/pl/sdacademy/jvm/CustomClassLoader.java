package pl.sdacademy.jvm;

public class CustomClassLoader extends ClassLoader {
    public CustomClassLoader(ClassLoader classLoader) {
        super(classLoader);
    }

    @Override
    public Class<?> loadClass(String s) throws ClassNotFoundException {
        System.out.println(s);
        return super.loadClass(s);
    }
}
