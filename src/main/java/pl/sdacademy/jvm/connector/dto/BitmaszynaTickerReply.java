package pl.sdacademy.jvm.connector.dto;

import java.io.Serializable;

public class BitmaszynaTickerReply implements Serializable {
    private double bid;
    private double ask;
    private double high;
    private double low;
    private double last;
    private double volume1;
    private double volume2;
    private double average;
    private double vwap;

    public double getBid() {
        return bid;
    }

    public BitmaszynaTickerReply setBid(double bid) {
        this.bid = bid;
        return this;
    }

    public double getAsk() {
        return ask;
    }

    public BitmaszynaTickerReply setAsk(double ask) {
        this.ask = ask;
        return this;
    }

    public double getHigh() {
        return high;
    }

    public BitmaszynaTickerReply setHigh(double high) {
        this.high = high;
        return this;
    }

    public double getLow() {
        return low;
    }

    public BitmaszynaTickerReply setLow(double low) {
        this.low = low;
        return this;
    }

    public double getLast() {
        return last;
    }

    public BitmaszynaTickerReply setLast(double last) {
        this.last = last;
        return this;
    }

    public double getVolume1() {
        return volume1;
    }

    public BitmaszynaTickerReply setVolume1(double volume1) {
        this.volume1 = volume1;
        return this;
    }

    public double getVolume2() {
        return volume2;
    }

    public BitmaszynaTickerReply setVolume2(double volume2) {
        this.volume2 = volume2;
        return this;
    }

    public double getAverage() {
        return average;
    }

    public BitmaszynaTickerReply setAverage(double average) {
        this.average = average;
        return this;
    }

    public double getVwap() {
        return vwap;
    }

    public BitmaszynaTickerReply setVwap(double vwap) {
        this.vwap = vwap;
        return this;
    }
}
