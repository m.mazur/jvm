package pl.sdacademy.jvm.connector

import spock.lang.Specification

class BitcoinMarketConnectorTest extends Specification {
    def "connector should return price"() {
        given:
        BitcoinMarketConnector connector = ConnectorProvider.bitcoinMarketConnector
        when:
        BigInteger result = connector.getBitcoinPrice()
        then:
        result != null
    }
}
