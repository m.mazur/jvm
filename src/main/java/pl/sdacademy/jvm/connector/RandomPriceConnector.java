package pl.sdacademy.jvm.connector;

import java.math.BigInteger;
import java.util.Random;

@DoNotUseOnProd
public class RandomPriceConnector implements BitcoinMarketConnector {
    public BigInteger getBitcoinPrice() {
        long price = (long) (new Random().nextFloat() * 100000 + 100000);
        return BigInteger.valueOf(price);
    }
}
