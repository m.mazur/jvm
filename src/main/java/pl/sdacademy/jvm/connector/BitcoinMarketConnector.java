package pl.sdacademy.jvm.connector;

import java.math.BigInteger;

/*
Task:

1. Napisz klasę, która implementuje interfejs BitcoinMarketConnector,
   - metoda getBitcoinPrice ma zwracać wartość losową od 100000 do 200000
   - podmień wpis 'connector' w pliku 'some.properties'
 */

public interface BitcoinMarketConnector {
    BigInteger getBitcoinPrice();
}
