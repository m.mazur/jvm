package pl.sdacademy.jvm.connector;

import java.math.BigInteger;

public class DummyConnector implements BitcoinMarketConnector {
    @Override
    public BigInteger getBitcoinPrice() {
        return BigInteger.valueOf(10000L);
    }
}
