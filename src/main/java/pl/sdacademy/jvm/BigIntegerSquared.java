package pl.sdacademy.jvm;

import java.math.BigInteger;

public class BigIntegerSquared {
    private BigInteger value;

    public BigIntegerSquared(BigInteger i) {
        this.value = i;
        this.value = value.multiply(value);
    }

    @Override
    public String toString() {
        return "BigIntegerSquared{" +
                "value=" + value +
                '}';
    }
}
