package pl.sdacademy.jvm;

import java.math.BigInteger;

public class Launcher {
    private static void goForward(int i) {
//        if (i == 10000) {
//            throw new RuntimeException();
//        }

        goForward(i+1);
    }

    public static void main(String[] args) {
//        BitcoinMarketConnector connector = ConnectorProvider.getBitcoinMarketConnector();

//        System.out.println(connector.getBitcoinPrice());
//        goForward(0);
        BigInteger i = BigInteger.ONE;

        while (true) {
            BigIntegerSquared bigIntegerSquared = new BigIntegerSquared(i);
            i = i.add(BigInteger.ONE);
            System.out.println(bigIntegerSquared);
        }
    }

}
